# cinema

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Version Node
```
Version Node 14.17.6
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
